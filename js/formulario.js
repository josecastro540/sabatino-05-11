$('input').iCheck({
  checkboxClass: 'icheckbox_minimal',
  radioClass: 'iradio_flat',
  increaseArea: '20%'
});

var color = $('#color');
var body = $('body');

color.change(function(){
  cambioColor();
})

function cambioColor(){
  body.css('background-color', color.val());
}
